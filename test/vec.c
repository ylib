/* Copyright (c) 2012-2015 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "vec.h"
#include <assert.h>
#include <stdlib.h>
#include <string.h>

#define A(v, ...) do {\
		int _t[] = {__VA_ARGS__};\
		assert(v.n == sizeof(_t)/sizeof(int));\
		assert(v.m >= v.n);\
		assert(memcmp(v.a, _t, sizeof(_t)) == 0);\
	} while(0)

#define CLEAR do {\
		vec_clear(v);\
		assert(v.n == 0);\
		assert(v.m == 0);\
		assert(v.a == NULL);\
	} while(0)


static void t_simple() {
	vec_t(int) v = {0,0,0};
	*vec_insert_fastp(v, 0) = 1;      A(v, 1);
	*vec_insert_fastp(v, v.n) = 2;    A(v, 1, 2);
	*vec_insert_fastp(v, 2) = 3;      A(v, 1, 2, 3);
	*vec_insert_fastp(v, 1) = 4;      A(v, 1, 4, 3, 2);
	vec_remove_fast(v, 0);            A(v, 2, 4, 3);
	vec_remove_fast(v, 2);            A(v, 2, 4);
	vec_remove_fast(v, v.n-1);        A(v, 2);
	vec_insert_fast(v, v.n, 5);       A(v, 2, 5);
	CLEAR;
	vec_append(v, 1);                 A(v, 1);
	*vec_appendp(v) = 2;              A(v, 1, 2);
	vec_insert_order(v, 0, 0);        A(v, 0, 1, 2);
	*vec_insert_orderp(v, v.n) = 3;   A(v, 0, 1, 2, 3);
	vec_remove_order(v, 0);           A(v, 1, 2, 3);
	vec_remove_order(v, v.n);         A(v, 1, 2);
	CLEAR;
}


static void t_search() {
	vec_t(int) v = {0,0,0};
	size_t r;

	vec_search(v, (assert(0), 0), assert(0));

	vec_insert_order(v, v.n, 10);
	r = (size_t)-1;
	vec_search(v, (assert(i == 0), 0), r=i);
	assert(r == 0);

	vec_insert_order(v, v.n, 11);
	r = (size_t)-1;
	vec_search(v, (assert(i < v.n), v.a[i] - 11), r=i);
	assert(r == 1);

	vec_insert_order(v, v.n, 12);
	vec_search(v, (assert(i < v.n), v.a[i] - 12), r=i);
	assert(r == 2);
	vec_search(v, (assert(i < v.n), v.a[i] - 10), r=i);
	assert(r == 0);
	vec_search(v, (assert(i < v.n), v.a[i] - 11), r=i);
	assert(r == 1);

	vec_search_insert(v, r, (assert(i < v.n), v.a[i] - 0));
	assert(r == 0);
	vec_search_insert(v, r, (assert(i < v.n), v.a[i] - 13));
	assert(r == 3);
	vec_search_insert(v, r, (assert(i < v.n), v.a[i] - 12));
	assert(r == 2);

	/* Thouroughly test various array sizes to cover all possible cases */
	size_t test, tests[] = {10,11,33,50,100,101};
	for(test=0; test<sizeof(tests)/sizeof(*tests); test++) {
		size_t cnt = tests[test];
		CLEAR;
		size_t i;
		for(i=0; i<cnt; i++)
			vec_insert_order(v, i, (int)100*i+5);

		vec_search(v, (assert(i < v.n), v.a[i] - 0), assert(0));
		vec_search(v, (assert(i < v.n), v.a[i] - 100), assert(0));
		vec_search(v, (assert(i < v.n), v.a[i] - 1004), assert(0));
		vec_search(v, (assert(i < v.n), v.a[i] - 100000), assert(0));

		for(i=0; i<cnt; i++) {
			int k = (int)100*i+5;
			r = (size_t)-1;
			vec_search(v, (assert(i < v.n), v.a[i] - k), r=i);
			assert(r == i);
			assert(v.a[r] == k);

			r = (size_t)-1;
			vec_search_insert(v, r, (assert(i < v.n), v.a[i] - k));
			assert(r == i);

			r = (size_t)-1;
			vec_search_insert(v, r, (assert(i < v.n), v.a[i] - (k-1)));
			assert(r == i);

			r = (size_t)-1;
			vec_search_insert(v, r, (assert(i < v.n), v.a[i] - (k+1)));
			assert(r == i+1);
		}
	}

	CLEAR;
}


int main(int argc, char **argv) {
	t_simple();
	t_search();
	return 0;
}

/* vim: set noet sw=4 ts=4: */
