/* Copyright (c) 2015 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "list.h"
#include <assert.h>
#include <stdlib.h>
#include <string.h>

typedef struct node node_t;
struct node { node_t *next, *prev; };

#define T(l, ...) do {\
		node_t *a[] = {__VA_ARGS__};\
		assert(l.head == a[0]);\
		assert(l.tail == a[sizeof(a)/sizeof(*a)-1]);\
		size_t i, n = sizeof(a)/sizeof(*a);\
		node_t *c = a[0];\
		for(i=0; i<n; i++) {\
			assert(c->prev == (i > 0   ? a[i-1] : NULL));\
			assert(c->next == (i < n-1 ? a[i+1] : NULL));\
			c = c->next;\
		}\
	} while(0)

#define hT(l, ...) do {\
		node_t *a[] = {__VA_ARGS__};\
		assert(l == a[0]);\
		size_t i, n = sizeof(a)/sizeof(*a);\
		node_t *c = a[0];\
		for(i=0; i<n; i++) {\
			assert(c->prev == (i > 0   ? a[i-1] : NULL));\
			assert(c->next == (i < n-1 ? a[i+1] : NULL));\
			c = c->next;\
		}\
	} while(0)

static void t_doubly() {
	struct { node_t *head, *tail; } l = {NULL,NULL};
	node_t m[5], *n[] = {m, m+1, m+2, m+3, m+4};

	list_prepend(l, n[0]);                   T(l, n[0]);
	list_insert_before(l, n[1], n[0]);       T(l, n[1], n[0]);
	list_insert_before(l, n[2], l.tail);     T(l, n[1], n[2], n[0]);
	list_remove(l, n[1]);                    T(l, n[2], n[0]);
	list_remove(l, n[0]);                    T(l, n[2]);
	list_append(l, n[3]);                    T(l, n[2], n[3]);
	list_insert_after(l, n[4], n[2]);        T(l, n[2], n[4], n[3]);
	list_remove(l, n[4]);                    T(l, n[2], n[3]);

	node_t *hl = NULL;
	hlist_prepend(hl, n[0]);                 hT(hl, n[0]);
	hlist_insert_before(hl, n[1], n[0]);     hT(hl, n[1], n[0]);
	hlist_insert_before(hl, n[2], n[0]);     hT(hl, n[1], n[2], n[0]);
	hlist_remove(hl, n[1]);                  hT(hl, n[2], n[0]);
	hlist_remove(hl, n[0]);                  hT(hl, n[2]);
	hlist_insert_after(hl, n[3], n[2]);      hT(hl, n[2], n[3]);
	hlist_insert_after(hl, n[4], n[2]);      hT(hl, n[2], n[4], n[3]);
	hlist_remove(hl, n[4]);                  hT(hl, n[2], n[3]);
}


int main(int argc, char **argv) {
	t_doubly();
	return 0;
}

/* vim: set noet sw=4 ts=4: */
